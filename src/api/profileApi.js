/**
 * Created by sawadam7108 on 7/7/2019.
 */
let profileList = {
    profiles: [{
        id:1,
        firstName: "Alex",
        lastName:"Merzer"
    }],
    index: 1
};

class profileApi {
    static getAllProfiles() {
        return new Promise((resolve, reject) => {
            setTimeout(()=>{
                resolve(profileList);
            }, 0)
        });
    }

    static deleteProfile(id) {
        return new Promise((resolve, reject) => {
            setTimeout(()=>{
                resolve(id);
            }, 0)
        });
    }

    static saveProfile(Profile) {
        return new Promise((resolve, reject) => {
            setTimeout(()=>{
                resolve(Profile);
            }, 0)
        });
    }

    static addProfile(Profile) {
        return new Promise((resolve, reject) => {
            setTimeout(()=>{
                resolve(Profile);
            }, 0)
        });
    }
}

export default profileApi;
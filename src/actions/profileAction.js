/**
 * Created by sawadam7108 on 7/7/2019.
 */
import * as actionTypes from './actionTypes'

export const loadProfiles = () => {
    return {
        type: actionTypes.LOAD_PROFILE_REQUEST
    }
}

export const saveProfile = (id, firstName, lastName) => {
    return {
        type: actionTypes.SAVE_PROFILE_REQUEST,
        payload: {id,firstName,lastName}
    }
}


export const addProfile = (firstName, lastName) => {
    return {
        type: actionTypes.ADD_PROFILE_REQUEST,
        payload: {firstName,lastName}
    }
}

export const deleteProfile = (id) => {
    return {
        type: actionTypes.DELETE_PROFILE_REQUEST,
        payload: id
    }
}
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {addProfile} from '../actions/profileAction';
import EditProfile from './editProfile'
import ProfileList from './profileList'

class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
    	const { addProfile } = this.props.actions
        console.log(this.props.profiles)
        return (
			<div className="container">
				<EditProfile addProfile={ addProfile } />
				{/*<ProfileList />*/}
			</div>
        )
    }
}

App.propTypes = {
    profiles: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
}

const mapDispatchToProps = (dispatch) => (
    {
        actions: bindActionCreators({addProfile}, dispatch)
    }
)

const mapStateToProps = (state) => (
    {
        profiles: state.profiles
    }
)

export default connect(mapStateToProps,mapDispatchToProps)(App);
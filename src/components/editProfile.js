/**
 * Created by sawadam7108 on 7/7/2019.
 */
import React from 'react';
import PropTypes from 'prop-types';

class editProfile extends React.Component {
    constructor(props) {
        super(props)

        this.handleClick = this.handleClick.bind(this)
        this.setValue = this.setValue.bind(this);
        this.state = {
            addProfileForm: null,
            errorLabel: {
                innerHTML : null
            },
            firstName:"",
            lastName:""
        }
    }

    handleClick (e) {
        const { addProfile } = this.props
        const { firstName, lastName} = this.state
        e.preventDefault();
        addProfile(firstName,lastName)
    }

    handleSave() {

    }

    setValue(value,type) {
        switch(type){
            case "FIRST_NAME" :
                this.setState({firstName:value})
                break;
            case "LAST_NAME" :
                this.setState({lastName:value})
                break;
        }
    }

    render() {
        let {addProfileForm, errorLabel } = this.state
        let { profile: { firstName, lastName } } = this.props
            return (
                <div className="AddProfile">
                    <form ref={(form) => {addProfileForm = form}} onSubmit={this.handleClick}>
                        <table style={{width:"100%"}}>
                            <tbody>
                            <tr>
                                <td>
                                    <label ref={(label) => {errorLabel = label}}>First Name</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" placeholder="Title" onChange={(e) => this.setValue(e.target.value,"FIRST_NAME")}
                                           defaultValue={firstName} style={{width:"30%"}} required/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label ref={(label) => {errorLabel = label}}>Last Name</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" placeholder="Title" onChange={(e) => this.setValue(e.target.value,"LAST_NAME")}
                                           defaultValue={lastName} style={{width:"30%"}} required/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="Submit" style={{width:"10%"}} />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            )
    }
}

editProfile.propTypes = {
    profile: PropTypes.object,
    addProfile: PropTypes.func.isRequired
}

editProfile.defaultProps = {
    profile: {
        id: 0,
        firstName: "",
        lastName: ""
    },
}

export default editProfile;
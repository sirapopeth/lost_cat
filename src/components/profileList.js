/**
 * Created by sawadam7108 on 7/7/2019.
 */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

class profileList extends React.Component {
    constructor(props) {
        super(props)

    }


    render() {
        const { profiles } = this.props.profiles
        console.log("profiles", profiles)
        return (
            <div className="comment">
                <div className="actions">
                    {profiles && profiles.map( profile => (
                        <input  key={profile.id} type="text" value={profile.firstName} />
                    ))}
                </div>
            </div>
        )
    }
}

profileList.propTypes = {
    profiles: PropTypes.object.isRequired
}

const mapStateToProps = (state) => (
    {
        profiles: state.profiles
    }
)

export default connect(mapStateToProps)(profileList);
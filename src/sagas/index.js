import { put, call, fork, takeEvery, all } from 'redux-saga/effects';
import profileApi from '../api/profileApi';
import * as actionTypes from '../actions/actionTypes';

function* loadProfiles(){
    try {
        const profiles = yield call(profileApi.getAllProfiles);
        yield put({type:actionTypes.LOAD_PROFILE_SUCCESS, profiles})
        
    } catch(e) {
        yield put({type:actionTypes.LOAD_PROFILE_FAILED, e})
    }
}

function* watchLoadProfiles(){
    yield takeEvery(actionTypes.LOAD_PROFILE_REQUEST, loadProfiles);
}

function* saveProfile(action){
    try {
        const profile = yield call(profileApi.saveProfile,action.payload);
        yield put({type: actionTypes.SAVE_PROFILE_SUCCESS, profile})
    } catch(e) {
        yield put({type: actionTypes.SAVE_PROFILE_FAILED, e})
    }
}

function* watchSaveProfile(){
    yield takeEvery(actionTypes.SAVE_PROFILE_REQUEST, saveProfile);
}

function* addProfile(action){
    try {
        const profile = yield call(profileApi.addProfile,action.payload);
        yield put({type: actionTypes.ADD_PROFILE_SUCCESS, profile})
    } catch(e) {
        yield put({type: actionTypes.ADD_PROFILE_FAILED, e})
    }
}

function* watchAddProfile(){
    yield takeEvery(actionTypes.ADD_PROFILE_REQUEST, addProfile);
}

function* deleteProfile(action){
    try {
        const id = yield call(profileApi.deleteProfile,action.payload);
        yield put({type: actionTypes.DELETE_PROFILE_SUCCESS, id})
    } catch(e) {
        yield put({type: actionTypes.DELETE_PROFILE_FAILED, e})
    }
}

function* watchDeleteProfile(){
    yield takeEvery(actionTypes.DELETE_PROFILE_REQUEST, deleteProfile);
}

export default function* rootSaga(){
    yield all([
        fork(watchLoadProfiles),
        fork(watchSaveProfile),
        fork(watchAddProfile),
        fork(watchDeleteProfile)
    ]);
}


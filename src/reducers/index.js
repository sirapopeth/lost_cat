import { combineReducers } from 'redux';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import profileReducer from './profileReducer';

export default combineReducers({
    profiles: profileReducer,
    router: routerReducer
});

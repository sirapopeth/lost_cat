import * as actionTypes from '../actions/actionTypes'
import initialState from './initialState'

export default function profileReducer(state = initialState.profileList, action){
    switch(action.type){
        case actionTypes.LOAD_PROFILE_SUCCESS:
            return [...state.profiles]
        case actionTypes.DELETE_PROFILE_SUCCESS:
            return state.filter((profile,i) => {
                return profile.id !== action.id
            })
        // case actionTypes.ADD_PROFILE_SUCCESS:
        //     console.log(action)
        //     let { index } = state
        //     return {
        //         ...state,
        //         index: index + 1,
        //         profiles: [...state.profiles, {
        //             id: index + 1,
        //             firstName: action.profile.firstName,
        //             lastName: action.profile.lastName
        //         }]
        //     }
        case actionTypes.SAVE_PROFILE_SUCCESS:
            return state.map((profile,i)=>{
                if(profile.id === action.profile.id){
                    return action.profile
                } else {
                    return profile
                }
            })
        default:
            return state
    }
}